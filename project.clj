(defproject uscensus "0.1.0-SNAPSHOT"
  :description "A simple wrapper for interacting with the US Census API"
  :url "https://gitlab.com/sigmavirus24/uscensus"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [clj-http "0.4.0"]
                 [cheshire "4.0.0"]]
  :profiles {:dev {:dependencies [[betamax "0.1.3"]]}})
