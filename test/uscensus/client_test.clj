(ns uscensus.client-test
  (:require [clojure.test :refer :all]
            [uscensus.client :refer :all]
            [betamax.core :as betamax]))

(betamax/configure "test/fixtures/cassettes")

(deftest discover-data-test
  (betamax/with-cassette "discover-data-json"
    (testing "The body is converted to a vector of hash-maps"
      (let [resp (discover-data)]
        (is (vector? (:body resp)))
        (is (map? (first (:body resp)))))))
  (betamax/with-cassette "discover-data-xml"
    (testing "The body remains a string"
      (let [resp (discover-data :xml)]
        (is (string? (:body resp)))))))
