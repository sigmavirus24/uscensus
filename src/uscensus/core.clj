(ns uscensus.core
  (:require [clj-http.client :as http]
            [clojure.set]))

(defn- parse-json-body
  "Make a GET request to the specified url"
  [response]
  (assoc response :body (http/json-decode (get response :body ""))))

(defn- create-keyword-map
  "Return a body's keys as a map of {old-keyword new-keyword}"
  [body]
  (reduce #(assoc %1 %2 (keyword %2)) {} (keys body)))

(defn- rename-body-keys
  "Updates a single body hash-map to use symbols as keywords instead of strings"
  [body]
  (clojure.set/rename-keys body (create-keyword-map body)))

(defn- rename-multiple-body-keys
  "Updates a vector of hash-maps to rename the keys to symbols"
  [bodies]
  (mapv rename-body-keys bodies))

(defn- json-transform
  "Transform a response's body to use symbols as keys"
  [response]
  (let [body (get (parse-json-body response) :body)]
    (if (vector? body)
      (assoc response :body (rename-multiple-body-keys body))
      (assoc response :body (rename-body-keys body)))))

(defn api-request
  [path data-format]
  (when-not (contains? #{:xml :json} data-format)
    (throw (IllegalArgumentException. "Data format must be :json or :xml")))
  (let [url (format "http://api.census.gov/%s.%s" path (name data-format))
        resp (http/get url {:throw-exceptions false})]
    (cond
     (= :json data-format) (json-transform resp)
     (= :xml data-format) resp
     :else nil)))
