(ns uscensus.client
  (:require [uscensus.core :as core]))

(defn discover-data
  "Access the US Census' Discovery Tool.
  Supported formats are :xml and :json. If no format is specified, :json will be used."
  ([] (discover-data :json))
  ([format] (core/api-request "data" format)))
